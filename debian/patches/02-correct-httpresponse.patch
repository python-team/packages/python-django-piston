From 90aefe87d08663dde1257f0f3ae8240d04d8741a Mon Sep 17 00:00:00 2001
From: Andi Albrecht <Andi Albrecht>
Date: Thu, 8 Oct 2015 11:49:44 -0700
Subject: Use _base_content_is_iter or _is_string

 Correctly use _base_content_is_iter or _is_string on HttpResponse depending on
 Django version.
Origin: upstream, https://bitbucket.org/jespern/django-piston/changeset/3a0d021dd042

Patch-Name: 02-correct-httpresponse.patch
---
 piston/resource.py | 23 ++++++++++++++++++-----
 piston/utils.py    | 28 ++++++++++++++++++----------
 2 files changed, 36 insertions(+), 15 deletions(-)

diff --git a/piston/resource.py b/piston/resource.py
index fbaed63..6243f47 100644
--- a/piston/resource.py
+++ b/piston/resource.py
@@ -1,5 +1,6 @@
 import sys, inspect
 
+import django
 from django.http import (HttpResponse, Http404, HttpResponseNotAllowed,
     HttpResponseForbidden, HttpResponseServerError)
 from django.views.debug import ExceptionReporter
@@ -181,13 +182,15 @@ class Resource(object):
         # If we're looking at a response object which contains non-string
         # content, then assume we should use the emitter to format that 
         # content
-        if isinstance(result, HttpResponse) and not result._is_string:
+        if self._use_emitter(result):
             status_code = result.status_code
-            # Note: We can't use result.content here because that method attempts
-            # to convert the content into a string which we don't want. 
-            # when _is_string is False _container is the raw data
+            # Note: We can't use result.content here because that
+            # method attempts to convert the content into a string
+            # which we don't want.  when
+            # _is_string/_base_content_is_iter is False _container is
+            # the raw data
             result = result._container
-     
+
         srl = emitter(result, typemapper, handler, fields, anonymous)
 
         try:
@@ -212,6 +215,16 @@ class Resource(object):
             return e.response
 
     @staticmethod
+    def _use_emitter(result):
+        """True iff result is a HttpResponse and contains non-string content."""
+        if not isinstance(result, HttpResponse):
+            return False
+        elif django.VERSION >= (1, 4):
+            return not result._base_content_is_iter
+        else:
+            return result._is_string
+
+    @staticmethod
     def cleanup_request(request):
         """
         Removes `oauth_` keys from various dicts on the
diff --git a/piston/utils.py b/piston/utils.py
index d8461d8..3439686 100644
--- a/piston/utils.py
+++ b/piston/utils.py
@@ -1,4 +1,6 @@
 import time
+
+import django
 from django.http import HttpResponseNotAllowed, HttpResponseForbidden, HttpResponse, HttpResponseBadRequest
 from django.core.urlresolvers import reverse
 from django.core.cache import cache
@@ -50,24 +52,30 @@ class rc_factory(object):
 
         class HttpResponseWrapper(HttpResponse):
             """
-            Wrap HttpResponse and make sure that the internal _is_string 
-            flag is updated when the _set_content method (via the content 
-            property) is called
+            Wrap HttpResponse and make sure that the internal
+            _is_string/_base_content_is_iter flag is updated when the
+            _set_content method (via the content property) is called
             """
             def _set_content(self, content):
                 """
-                Set the _container and _is_string properties based on the 
-                type of the value parameter. This logic is in the construtor
-                for HttpResponse, but doesn't get repeated when setting 
-                HttpResponse.content although this bug report (feature request)
-                suggests that it should: http://code.djangoproject.com/ticket/9403 
+                Set the _container and _is_string /
+                _base_content_is_iter properties based on the type of
+                the value parameter. This logic is in the construtor
+                for HttpResponse, but doesn't get repeated when
+                setting HttpResponse.content although this bug report
+                (feature request) suggests that it should:
+                http://code.djangoproject.com/ticket/9403
                 """
+                is_string = False
                 if not isinstance(content, basestring) and hasattr(content, '__iter__'):
                     self._container = content
-                    self._is_string = False
                 else:
                     self._container = [content]
-                    self._is_string = True
+                    is_string = True
+                if django.VERSION >= (1, 4):
+                    self._base_content_is_iter = not is_string
+                else:
+                    self._is_string = is_string
 
             content = property(HttpResponse._get_content, _set_content)            
 
